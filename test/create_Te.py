import numpy as np
import matplotlib.pyplot as plt

Nx = 151
Ny = 201
Lx = 1300.0E3
Ly = 1800.0E3

x = np.linspace(0,Lx,Nx)
y = np.linspace(0,Ly,Ny)

X,Y = np.meshgrid(x,y)


load = np.zeros((Ny,Nx))
cond = (X>Lx/2-Lx/8)&(X<Lx/2+Lx/8)&(Y>Ly/2-Ly/8)&(Y<Ly/2+Ly/8)

h = 3000.0
g = 9.8
rho = 2700.0

load[cond] = -h*g*rho

plt.imshow(load,extent=(0,Lx/1000,0,Ly/1000))
plt.colorbar()
plt.savefig("load.png")
plt.close()

load = np.transpose(load)
load = np.reshape(load,(Nx)*(Ny))
np.savetxt("load_map.txt",load, fmt="%f")
"""
XX = np.transpose(X)
XX = np.reshape(XX,Nx*Ny)
np.savetxt("X.txt",XX,fmt="%f")

YY = np.transpose(Y)
YY = np.reshape(YY,Nx*Ny)
np.savetxt("Y.txt",YY,fmt="%f")
"""

######################


dx = Lx/(Nx-1)
dy = Ly/(Ny-1)

x = np.linspace(dx/2,Lx-dx/2,Nx-1)
y = np.linspace(dy/2,Ly-dy/2,Ny-1)

X,Y = np.meshgrid(x,y)
Te = X*0 + 20000.0

cond = X<Lx/2
Te[cond]=10000.0


plt.imshow(Te,extent=(0,Lx/1000,0,Ly/1000))
plt.colorbar()
plt.savefig("Te.png")
plt.close()

Te = np.transpose(Te)
Te = np.reshape(Te,(Nx-1)*(Ny-1))

np.savetxt("Te_map.txt",Te, fmt="%f")

f = open("param.txt", "w")
f.write("%d %d\n%f %f\n%f %f\n"%(Nx,Ny,0,Lx,0,Ly))
f.close()

rhom = 3300.0
rhos = 1000.0
E = 1.0E11
nu = 0.25


f = open("properties.txt", "w")
f.write("%lf %lf %lf %lf %lf\n"%(E,nu,rhos,rhom,g))
f.close()

"""
XX = np.transpose(X)
XX = np.reshape(XX,(Nx-1)*(Ny-1))
np.savetxt("X_Te.txt",XX,fmt="%f")

YY = np.transpose(Y)
YY = np.reshape(YY,(Nx-1)*(Ny-1))
np.savetxt("Y_Te.txt",YY,fmt="%f")
"""