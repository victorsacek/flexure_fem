import numpy as np
import matplotlib.pyplot as plt

with open("param.txt","r") as f:
    line = f.readline()
    line = line.split()
    Nx = int(line[0])
    Ny = int(line[1])
    line = f.readline()
    line = line.split()
    minx = float(line[0])
    maxx = float(line[1])
    line = f.readline()
    line = line.split()
    miny = float(line[0])
    maxy = float(line[1])


w = np.loadtxt("wflex.txt")

w = np.reshape(w,(Nx,Ny))
w = np.transpose(w)

plt.imshow(w,extent=(minx/1000,maxx/1000,miny/1000,maxy/1000))
plt.colorbar()
plt.savefig("w.png")

plt.close()