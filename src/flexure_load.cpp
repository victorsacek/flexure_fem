#include <stdio.h>
#include <stdlib.h>
#define NODE_POR_ELE 3
#define GLNODE 3

void montaf (double *load,double xi,double xj,double xk,double yi,double yj,double yk, 
             double q1, long ni, long nj, long nk,double L1,double L2,double L3);

void montaf3(double *load,double xi,double xj,double xk,double yi,
			 double yj,double yk, double q1, double q2, double q3, long ni, long nj, long nk);



extern double *bflex;

extern long tri_flex;
extern long **Tri_flex;
extern long nodes_flex;
extern double **xy_flex;

extern double area_ele_flex;

extern double RHOW;
extern double RHOS;
extern double RHOC;
extern double RHOM;


extern double *load_map;


void flexure_load()
{
    long i,fi,j,t;
    long ni,nj,nk;

	double xi,xj,xk,yi,yj,yk,q1,q2,q3;
    
    for (fi=0;fi<nodes_flex;fi++){
        for (j=0;j<3;j++){
            bflex[fi*3+j]=0;
        }
    }
	

    for (t=0;t<tri_flex;t++){
        xi = xy_flex[Tri_flex[t][0]][0];
        xj = xy_flex[Tri_flex[t][1]][0];
        xk = xy_flex[Tri_flex[t][2]][0];
        yi = xy_flex[Tri_flex[t][0]][1];
        yj = xy_flex[Tri_flex[t][1]][1];
        yk = xy_flex[Tri_flex[t][2]][1];

        ni = Tri_flex[t][0];
        nj = Tri_flex[t][1];
        nk = Tri_flex[t][2];

        q1 = load_map[ni];
        q2 = load_map[nj];
        q3 = load_map[nk];
        

        montaf3(bflex,xi,xj,xk,yi,yj,yk, q1,q2,q3,  ni,  nj,  nk);
    }
	
    
}

void montaf (double *load,double xi,double xj,double xk,double yi,double yj,double yk, 
             double q1, long ni, long nj, long nk,double L1,double L2,double L3)
{
	double N[9];
	double b1,b2,b3,c1,c2,c3,l1,l2,l3,mi1,mi2,mi3,delta;
	b1=yj-yk; c1=xk-xj;
	b2=yk-yi; c2=xi-xk;
	b3=yi-yj; c3=xj-xi;
	l1=(xk-xj)*(xk-xj)+(yk-yj)*(yk-yj);
	l2=(xk-xi)*(xk-xi)+(yk-yi)*(yk-yi);
	l3=(xi-xj)*(xi-xj)+(yi-yj)*(yi-yj);
	mi1=(l3-l2)/l1;
	mi2=(l1-l3)/l2;
	mi3=(l2-l1)/l3;
	delta=0.5*((xj*yk-yj*xk)+xi*(yj-yk)+yi*(xk-xj));
	if (delta==0) {
        printf("Problemas! delta=0!"); 
        printf("%f %f %f %f %f %f\n",xi,xj,xk, yi,yj,yk);
        printf("%ld %ld %ld",ni,nj,nk);
        exit(-1);
    }
	if (delta<0) {printf("Que estranho"); delta=delta*(-1); }


	N[0]=2*(-L1*L3*L3+L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2-L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2+L1*L1*L2)+L1*L3-L1*L2+L1;
	N[1]=-b2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L3)-b3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2);
	N[2]=-c2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L3)-c3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2);
	N[3]=2*(-L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L1*L1*L2)-L2*L3+L1*L2+L2;
	N[4]=-b3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2-L1*L2)-b1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3);
	N[5]=-c3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2-L1*L2)-c1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3);
	N[6]=2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2-L2*L2*L3)+L2*L3-L1*L3+L3;
	N[7]=-b2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2)-b1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L2*L3);
	N[8]=-c2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2)-c1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L2*L3);
	
	load[ni*3+0]+= N[0]*q1;
	load[ni*3+1]+= N[1]*q1;
	load[ni*3+2]+= N[2]*q1;
	load[nj*3+0]+= N[3]*q1;
	load[nj*3+1]+= N[4]*q1;
	load[nj*3+2]+= N[5]*q1;
	load[nk*3+0]+= N[6]*q1;
	load[nk*3+1]+= N[7]*q1;
	load[nk*3+2]+= N[8]*q1; /**/
  	
	
	/*for(cont=0,q1=q1/delta; cont<3; cont++){
		
		if(cont==0) { L1=0; 	L2=0.5; 	L3=0.5; }
		if(cont==1) { L1=0.5; 	L2=0; 		L3=0.5; }
		if(cont==2) { L1=0.5; 	L2=0.5; 	L3=0; }
		
		N[0]=2*(-L1*L3*L3+L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2-L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2+L1*L1*L2)+L1*L3-L1*L2+L1;
    	N[1]=-b2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L3)-b3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2);
		N[2]=-c2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L3)-c3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2);
		N[3]=2*(-L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L1*L1*L2)-L2*L3+L1*L2+L2;
		N[4]=-b3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2-L1*L2)-b1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3);
		N[5]=-c3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2-L1*L2)-c1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3);
		N[6]=2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2-L2*L2*L3)+L2*L3-L1*L3+L3;
		N[7]=-b2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2)-b1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L2*L3);
		N[8]=-c2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2)-c1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L2*L3);
		
		load[ni*3+0] = load[ni*3+0] + N[0]*(q1)*delta/3;
		load[ni*3+1] = load[ni*3+1] + N[1]*(q1)*delta/3;
		load[ni*3+2] = load[ni*3+2] + N[2]*(q1)*delta/3;
		load[nj*3+0] = load[nj*3+0] + N[3]*(q1)*delta/3;
		load[nj*3+1] = load[nj*3+1] + N[4]*(q1)*delta/3;
		load[nj*3+2] = load[nj*3+2] + N[5]*(q1)*delta/3;
		load[nk*3+0] = load[nk*3+0] + N[6]*(q1)*delta/3;
		load[nk*3+1] = load[nk*3+1] + N[7]*(q1)*delta/3;
		load[nk*3+2] = load[nk*3+2] + N[8]*(q1)*delta/3;
	}*/



}


void montaf3(double *load,double xi,double xj,double xk,double yi,
			double yj,double yk, double q1, double q2, double q3, long ni, long nj, long nk)
{
	long cont;
	double N[9], L1, L2, L3;
	double b1,b2,b3,c1,c2,c3,l1,l2,l3,mi1,mi2,mi3,delta;
	b1=yj-yk; c1=xk-xj;
	b2=yk-yi; c2=xi-xk;
	b3=yi-yj; c3=xj-xi;
	l1=(xk-xj)*(xk-xj)+(yk-yj)*(yk-yj);
	l2=(xk-xi)*(xk-xi)+(yk-yi)*(yk-yi);
	l3=(xi-xj)*(xi-xj)+(yi-yj)*(yi-yj);
	mi1=(l3-l2)/l1;
	mi2=(l1-l3)/l2;
	mi3=(l2-l1)/l3;
	delta=0.5*((xj*yk-yj*xk)+xi*(yj-yk)+yi*(xk-xj));
	if (delta==0) exit(-1);
	if (delta<0) delta=delta*(-1);
	
	for(cont=0; cont<3; cont++){
		
		if(cont==0) { L1=0; 	L2=0.5; 	L3=0.5; }
		if(cont==1) { L1=0.5; 	L2=0; 		L3=0.5; }
		if(cont==2) { L1=0.5; 	L2=0.5; 	L3=0; }
		
		N[0]=2*(-L1*L3*L3+L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2-L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2+L1*L1*L2)+L1*L3-L1*L2+L1;
    	N[1]=-b2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L3)-b3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2);
		N[2]=-c2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L3)-c3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2);
		N[3]=2*(-L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L1*L1*L2)-L2*L3+L1*L2+L2;
		N[4]=-b3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2-L1*L2)-b1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3);
		N[5]=-c3*(L1*L2*L3*((3*mi3+1)*L3-(3*mi3+1)*L2+3*(1-mi3)*L1)/2+L1*L1*L2-L1*L2)-c1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3);
		N[6]=2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2-L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2-L2*L2*L3)+L2*L3-L1*L3+L3;
		N[7]=-b2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2)-b1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L2*L3);
		N[8]=-c2*(L1*L3*L3+L1*L2*L3*(3*(1-mi2)*L3+(3*mi2+1)*L2-(3*mi2+1)*L1)/2)-c1*(L1*L2*L3*(-(3*mi1+1)*L3+3*(1-mi1)*L2+(3*mi1+1)*L1)/2+L2*L2*L3-L2*L3);
		
		load[ni*3+0] = load[ni*3+0] + N[0]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[ni*3+1] = load[ni*3+1] + N[1]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[ni*3+2] = load[ni*3+2] + N[2]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[nj*3+0] = load[nj*3+0] + N[3]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[nj*3+1] = load[nj*3+1] + N[4]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[nj*3+2] = load[nj*3+2] + N[5]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[nk*3+0] = load[nk*3+0] + N[6]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[nk*3+1] = load[nk*3+1] + N[7]*(q1*L1+q2*L2+q3*L3)*delta/3;
		load[nk*3+2] = load[nk*3+2] + N[8]*(q1*L1+q2*L2+q3*L3)*delta/3;
	}
	
}
