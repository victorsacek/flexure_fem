#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "header.h"
#include <sys/time.h>

void mesh(double minx,double maxx,double miny,double maxy,long Nx, long Ny);

void flexureK();
void flexure_load();
void flexure_solv(double **Kflex, double *Kdiag, double *bflex, double *wflex);
void flexure_result(double *wflex,long Nx, long Ny);

int main()
{

    FILE *f_param;

    f_param = fopen("param.txt","r");
    fscanf(f_param,"%ld %ld %lf %lf %lf %lf",&Nx,&Ny,&minx,&maxx,&miny,&maxy);
    printf("%ld %ld %lf %lf %lf %lf\n",Nx,Ny,minx,maxx,miny,maxy);
    /*double minx=0.0;
    double maxx=300.0E3;
    double miny=0.0;
    double maxy=400.0E3;
    long Nx = 301;
    long Ny = 201;*/
    
    mesh(minx,maxx,miny,maxy,Nx,Ny);
    flexureK();
    flexure_load();
    flexure_solv(Kflex, Kdiag, bflex, wflex);

    flexure_result(wflex,Nx,Ny);

    return 0;
}


double **Aloc_matrix_real (long p, long n)
{
	double **v;
	long i;
	if(p < 1 || n<1){
		printf("** Erro: Parametro invalido **\n");
		return(NULL);
	}
	v = (double **) calloc(p, sizeof(double *));
	for(i=0; i<p; i++){
		v[i] = (double *) calloc (n, sizeof(double));
		if (v[i] == NULL) {
			printf("** Erro: Memoria Insuficiente **");
			return(NULL);
		}
	}
	return(v);
}
long **Aloc_matrix_long (long p, long n)
{
	long **v;
	long i;
	if(p < 1 || n<1){
		printf("** Erro: Parametro invalido **\n");
		return(NULL);
	}
	v = (long **) calloc(p, sizeof(long *));
	for(i=0; i<p; i++){
		v[i] = (long *) calloc (n, sizeof(long));
		if (v[i] == NULL) {
			printf("** Erro: Memoria Insuficiente **");
			return(NULL);
		}
	}
	return(v);
}
double *Aloc_vector_real (long n)
{
	double *v;
	v = (double *) calloc(n, sizeof(double));
	if (v == NULL){
		printf("* Erro: Memoria Insuficiente *");
		return(NULL);
	}
	return(v);
}

long *Aloc_vector_long (long n)
{
	long *v;
	v = (long *) calloc(n, sizeof(long));
	if (v == NULL){
		printf("* Erro: Memoria Insuficiente *");
		return(NULL);
	}
	return(v);
}
