#include <stdio.h>
#include <stdlib.h>

void flexure_result(double *wflex,long Nx, long Ny)
{
    FILE *f_out;

    f_out = fopen("wflex.txt","w");

    long i,j;

    for (i=0;i<Nx;i++){
		for (j=0;j<Ny;j++){
            fprintf(f_out,"%lf\n",wflex[(i*Ny+j)*3]);
        }
    }

    fclose(f_out);
}

