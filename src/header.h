
double TeConstante;
double TeConstante2;

double RHOM=3300.0;

long tri_flex;
long **Tri_flex;
long nodes_flex;
double **xy_flex;

long **cond_c;

double **Kflex;
double **Kflex_c;
long **Kconec;
long *Kposconec;

double *Kdiag;
double *Kdiag_c;

double **Ke;
double *Te;
double *props;

//double *qflex;
//double *qflex_a;
double *bflex;
double *uflex;
double *wflex;
double *wflex_aux;
double *wflex_cumula;
double *wflex_fault;

double *IsoT;

long max_conec_p=40;

double area_ele_flex;

double dx_flex;
double dy_flex;

double minx_flex;
double miny_flex;

double *load_map;

long Nx;
long Ny;

double minx;
double maxx;
double miny;
double maxy;